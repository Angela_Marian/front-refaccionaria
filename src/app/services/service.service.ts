import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class ServiceService { 

  private URL = "http://localhost:3900";

  constructor(private _http:HttpClient) { }

  //Personal
  consultapersonal(){
   const url=`${this.URL}/obtener/personal`;
   return this._http.get( url );
   console.log(url);
  }

  //Proveedores
  consultaproveedor(){
   const url=`${this.URL}/obtener/proveedor`;
   return this._http.get( url );
   console.log(url);
  }

  //Productos
  consultaproducto(){
       const url=`${this.URL}/obtener/producto`;
       return this._http.get( url );
       console.log(url);
      }

  //Sucursal
  consultasucursal(){
       const url=`${this.URL}/obtener/sucursal`;
       return this._http.get( url );
       console.log(url);
      }

  agregarproducto(body:any){
    const url=`${this.URL}/producto/nuevo`;
    return this._http.post(url,body);
    }

  agregarsucursal(body:any){
    const url=`${this.URL}/sucursal/nuevo`;
    return this._http.post(url,body);
    }

  agregarproveedores(body:any){
    const url=`${this.URL}/proveedor/nuevo`;
    return this._http.post(url,body);
    }
  agregarpersonal(body:any){
    const url=`${this.URL}/personal/nuevo`;
    return this._http.post(url,body);
    }
  
   
}

