import { Component, OnInit} from '@angular/core';
import { ServiceService } from 'src/app/services/service.service';
@Component({
  selector: 'app-compo1',
  templateUrl: './compo1.component.html',
  styleUrls: ['./compo1.component.css']
})
export class Compo1Component implements OnInit{
  datos1: any[] = []
  datos2: any[] = []
  datos3: any[] = []
  datos4: any[] = []

  producto = {
    nombreProducto:'', 
    marcaProducto:'',
    presentacionProducto:'', 
    contenidoProducto:'', 
    costoProducto:'', 
    proveedorProducto:'',
    cantidadIngresa:'',
    statusProducto:'',
    descripcionProducto:'', 
  }

  sucursal = {
    nombreSucursal: '',
    paisSucursal: '',
    direccionSucursal: '',
    telefonoSucursal: '',
    statusSucursal: '',
  }

  proveedor = {
    marcaProveedor: '',
    localizacionProveedor: '',
    telefonoProveedor: '',
    statusProveedor: '', 
    descripcionProveedor: '',
  }

  personal = {
    nombrePersonal: '',
    apellidopaPersonal: '',
    apellidomaPersonal: '',
    telefonoPersonal: '',
    statusPersonal: '',
  }
  
  constructor(private _service:ServiceService) { }

  ngOnInit(): void {
    this.consultaPersonal();
    this.consultaProveedores();
    this.consultaProductos();
    this.consultaSucursal();
  }
  //Personal
  consultaPersonal(){
    this._service.consultapersonal()
    .subscribe((resp:any) => {
      console.log(resp.respuesta);
      this.datos1 = resp.respuesta;
    })
  }

  //Proveedores
  consultaProveedores(){
    this._service.consultaproveedor()
    .subscribe((resp:any) => {
      console.log(resp.respuesta);
      this.datos2 = resp.respuesta;
    })
  }

    //Productos
    consultaProductos(){
      this._service.consultaproducto()
      .subscribe((resp:any) => {
        console.log(resp.respuesta);
        this.datos3 = resp.respuesta;
      })
    }

    //Sucursal
    consultaSucursal(){
      this._service.consultasucursal()
      .subscribe((resp:any) => {
        console.log(resp.respuesta);
        this.datos4 = resp.respuesta;
          })
        }
    //Guardarproducto
     guardarProducto(){
      if (this.producto.nombreProducto == '' || this.producto.marcaProducto == '' || this.producto.presentacionProducto == '' || this.producto.contenidoProducto == '' || this.producto.costoProducto == '' || this.producto.proveedorProducto == '' || this.producto.cantidadIngresa == '' || this.producto.statusProducto == '' || this.producto.descripcionProducto == '') {
        alert('Por favor llene los campos');
      }else{
      alert('Agregado correctamente');
      console.log(this.producto);
      this._service.agregarproducto(this.producto).subscribe((resp:any) => {
      })
    }
    }

    //GuardarSucursal
    guardarSucursales(){
      if (this.sucursal.nombreSucursal == '' || this.sucursal.paisSucursal == '' || this.sucursal.direccionSucursal == '' || this.sucursal.telefonoSucursal == '' || this.sucursal.statusSucursal == '') {
        alert('Por favor llene los campos');
      }else{
      alert('Agregado correctamente');
      console.log(this.sucursal);
      this._service.agregarsucursal(this.sucursal).subscribe((resp:any) => {
      })
    }
    }

    //GuardarProveedor
    guardarProveedores(){
      if (this.proveedor.marcaProveedor == '' || this.proveedor.localizacionProveedor == '' || this.proveedor.telefonoProveedor == '' || this.proveedor.statusProveedor == '' || this.proveedor.descripcionProveedor == '') {
        alert('Por favor llene los campos');
      }else{
      alert('Agregado correctamente');
      console.log(this.proveedor);
      this._service.agregarproveedores(this.proveedor).subscribe((resp:any) => {
      })
    }
    }

    //GuardarPersonal
    guardarPersonal(){
      if (this.personal.nombrePersonal == '' || this.personal.apellidopaPersonal == '' || this.personal.apellidomaPersonal == '' || this.personal.telefonoPersonal == '' || this.personal.statusPersonal == '') {
        alert('Por favor llene los campos');
      }else{
      alert('Agregado correctamente');
      console.log(this.personal);
      this._service.agregarpersonal(this.personal).subscribe((resp:any) => {
      })
    }
    }

 ///   deleteProductos(producto:Producto):void{    ///para hacer este me guie de un video por eso está así
  ///    console.log("Hello from delete");
  ///    this._service.deleteProducto(_id).subscribe(
  ///      res=>this._service.getAll().subscribe(
  ///        response=>this.personal=response
  //      )
  //    );
   /// }
}
